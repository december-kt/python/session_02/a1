year = int(input("Please input a year: "))

if year % 4 == 0:
	print(f"{year} is a leap year")
else:
	print(f"{year} is not a leap year")

row = int(input("Enter number of rows: "))
col = int(input("Enter number of columns: "))

for i in range(row):
	for j in range(col):
		print("*", end=" ")
	print("\n")